<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$convertedTemperature    = 0;


if($_SERVER['REQUEST_METHOD'] == 'POST'){
   $temperature            = $_POST['temperature'];
   $fromConvertionUnit     = $_POST['fromConvertionUnit'];
   $toConvertionUnit       = $_POST['toConvertionUnit'];



   if($fromConvertionUnit == $toConvertionUnit){
       $convertedTemperature = $temperature;
   }else if($fromConvertionUnit == 'fahrenheit' && $toConvertionUnit == 'celcius') {


       $convertedTemperature = ($temperature - 32) * 0.5556;
   }else if($fromConvertionUnit == 'celcius' && $toConvertionUnit == 'fahrenheit') {

       $convertedTemperature = ($temperature * 1.8) + 32;
   }
}
?>
<!doctype html>
<html lang="en">
   <head>
       <meta charset="UTF-8">
       <title>Temperature Converter</title>
   </head>
   <body>
       <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
           <div>

               <label for="temperature">Temperature</label> <br/>
               <input type="number" name="temperature" id="temperature" value="<?php echo (!empty($temperature)) ? $temperature : '' ?>">
           </div>
           <div>
               <label for="fromConvertionUnit">Select From Convertion Unit</label><br/>
               <select name="fromConvertionUnit" id="fromConvertionUnit">
                   <option value="fahrenheit">Fahrenheit</option>
                   <option value="celcius">Celcius</option>
               </select>
       </div>
       <div>
               <label for="toConvertionUnit">Select To Convertion Unit</label><br/>
               <select name="toConvertionUnit" id="toConvertionUnit">
                   <option value="fahrenheit">Fahrenheit</option>
                   <option value="celcius">Celcius</option>
               </select>
       </div>

           <div>
       Converted Temperature <b> <?php  echo (!empty($temperature)) ? $temperature : '--' ?></b> Value From   <b><?php echo  (!empty($fromConvertionUnit)) ? $fromConvertionUnit : '--' ?></b> TO  <b><?php echo  (!empty($toConvertionUnit)) ? $toConvertionUnit : '--' ?></b> is <b><?php echo  (!empty($convertedTemperature)) ? $convertedTemperature : '--' ?><b/>
               <label for="converted_value">Converted Value</label><br/>
<input type="number" value="<?php echo (!empty($convertedTemperature)) ? $convertedTemperature : '0' ?>">
           </div>
           <div>
<input type="submit" value="Convert">
           </div>
       </form>
   </body>
</html>
